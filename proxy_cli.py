import boto
import boto.ec2

from spotInstance import requestSpotInstance
from spotInstance import terminateInstances
from spotInstance import cancelSpotInstanceRequests

import settings
import harness
import os
import time
import logging

import shlex
import subprocess
import traceback

harness.initEnv()

os.environ['AWS_ACCESS_KEY_ID'] = settings.aws_access_key_id
os.environ['AWS_SECRET_ACCESS_KEY'] = settings.aws_secret_access_key

def get_local_ip(ifname):
    import socket, fcntl, struct
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    inet = fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))
    ret = socket.inet_ntoa(inet[20:24])
    return ret

def startProxy(args):
    instId = None
    #localIp = get_local_ip('eth0')
    localIp = 'localhost'

    try:
        conn = boto.ec2.connect_to_region(settings.region)

        inst = None
        insts = conn.get_only_instances(filters= {'instance-state-name': 'running'})

        if len(insts) == 0:
            instId = requestSpotInstance(conn)

        while len(insts) == 0:
            insts = conn.get_only_instances(filters= {'instance-state-name': 'running'})
            time.sleep(2)
            logging.debug("Pending for running instance...")

        while True:
            ret = conn.get_all_instance_status([i.id for i in insts])
            ret = [i.id for i in ret if i.system_status.status == 'ok']
            if len(ret) > 0:
                inst = conn.get_only_instances(ret)[0]
                break
            time.sleep(5)
            logging.debug("Pending for ok instance...")
        logging.info("Using: %s" % inst)

        ipAddr = inst.ip_address
        logging.info("IP ADDR: %s" % ipAddr)

        command = 'sslocal -s %s -p %d -b %s -l %d -k %s -m %s -t %d --fast-open' % (
                ipAddr, 443, localIp, 1080, '02165453174', 'rc4-md5', 300,
                )

        ret = subprocess.Popen(shlex.split(command), stdout=None, stderr=subprocess.PIPE)
        try:
            for line in iter(ret.stderr.readline, ''):
                logging.info(line.rstrip('\n'))
        except KeyboardInterrupt:
            pass
        except:
            raise
    except KeyboardInterrupt as e:
        logging.info('KeyboardInterrupt')
        logging.info(str(e))
    except Exception as e:
        logging.info('Exception')
        logging.info(str(e))
        if instId:
            ret = conn.terminate_instances([instId])
            logging.info('following instance has been terminated: %s' % ', '.join(ret))
    except:
        logging.error(traceback.format_exc())

def stopProxy(args):
    try:
        conn = boto.ec2.connect_to_region(settings.region)

        terminateInstances(conn)
        cancelSpotInstanceRequests(conn)

        logging.info('instances and spot instance requests have been terminated')

    except:
        logging.error(traceback.format_exc())


if __name__ == '__main__':
    harness.setupLogging()
    import argparse
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(title='subcommands')
    ret = subparser.add_parser('start')
    ret.set_defaults(func=startProxy)
    subparser.add_parser('stop').set_defaults(func=stopProxy)

    args = parser.parse_args()
    args.func(args)

