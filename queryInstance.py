import time
import datetime
import json

import boto
import boto.ec2

from subprocess import Popen

import settings
import harness


if __name__ == '__main__':
    import os
    os.environ['AWS_ACCESS_KEY_ID'] = settings.aws_access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = settings.aws_secret_access_key

    conn = boto.ec2.connect_to_region(settings.region)

