import os
import sys
import json
import logging
import logging.config

import subprocess
import shlex

def initEnv():
    realpath = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(realpath)

def setupLogging():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': True,

        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level':'DEBUG',
                'class':'logging.StreamHandler',
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'DEBUG',
                'propagate': True
            }
        }
    })

def call(command):
    ret = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ret.wait()

    (stdoutdata, stderrdata) = ret.communicate()
    if stderrdata:
        print 'stderr: ', stderrdata
        return None

    try:
        return json.loads(stdoutdata)
    except:
        return None
