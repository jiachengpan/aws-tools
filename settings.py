import os
from settings_local import aws_access_key_id
from settings_local import aws_secret_access_key

realpath = os.path.dirname(os.path.realpath(__file__))

region = 'ap-northeast-1'

USER_DATA_DIR = os.path.join(realpath, 'user_data/')

ss_user_data = {}

for filename in os.listdir(USER_DATA_DIR):
    with open(USER_DATA_DIR + filename) as fh:
        ss_user_data[filename] = fh.read()
