import time
import datetime
import json

import boto
import boto.ec2

from subprocess import Popen

import settings
import harness
import logging

_imageId    = 'ami-8d6d9d8d'
_keyName    = 'key-tokyo'
_userData   = settings.ss_user_data['shadowsocks.sh']
_instanceType       = 't1.micro'
_availabilityZone   = 'ap-northeast-1c'
_productDescript    = 'Linux/UNIX'
_securityGroupIds   = ["sg-3993275c"]

STATE_TERMINATED= ['active', 'closed', 'failed', 'canceled']
STATE_ACTIVE    = ['open']


def terminateInstances(conn, insts=None):
    if not insts:
        insts = conn.get_all_instances()

    if len(insts) == 0:
        logging.info('No running instance, quitting...')
        return

    logging.info('Terminating %r...' % insts)
    ret = conn.terminate_instances([i.id for i in insts])

    if len(ret) == len(insts):
        logging.info('All instances have been terminated')
    else:
        logging.error('Not all instances have been terminated.')

def cancelSpotInstanceRequests(conn, sirs=None):
    logging.info('Cleaning up...')
    if not sirs:
        sirs = conn.get_all_spot_instance_requests()

    if len(sirs) == 0:
        logging.info('No SIR, quitting...')
        return

    logging.info('Canceling %r...' % sirs)
    ret = conn.cancel_spot_instance_requests(
            [i.id for i in sirs if i.state not in STATE_TERMINATED])
    if len(ret) == len(sirs):
        logging.info('All SIRs have been canceled')
    else:
        logging.error('Not all SIRs have been canceled. Something went wrong...')

def pendForInstance(conn, requestIds=None):
    jobInstId = None
    while not jobInstId:
        # len(sirs) is supposed to be 1
        ret = conn.get_all_spot_instance_requests(request_ids=requestIds)

        if ret[0].state in STATE_TERMINATED:
            logging.debug('SIR is terminated... %s' % ret[0].state)
            if ret[0].state != 'active':
                return None
            while True:
                ret = conn.get_all_spot_instance_requests(request_ids=requestIds)
                if ret[0].status.code == 'fulfilled':
                    jobInstId = ret[0].instance_id
                    logging.debug('SIR is fulfilled! INST_ID: %s' % jobInstId)
                    return jobInstId
                logging.debug('Pending for request instance... %s' % ret[0].status)
                time.sleep(5)
        logging.debug('Pending for request to be fulfilled...')
        time.sleep(5)
    return None

def requestSpotInstance(conn,
        imageId=_imageId, keyName=_keyName, userData=_userData,
        instanceType=_instanceType, availabilityZone=_availabilityZone,
        productDescript=_productDescript, securityGroupIds=_securityGroupIds):
    '''
    Request spot instance
    By default: t1.micro in tokyo for vpn use
    '''

    sirs = []

    try:
        tNow = datetime.datetime.now()
        tAgo = tNow - datetime.timedelta(days=3)

        ret = conn.get_spot_price_history(start_time=tAgo.isoformat(),
                instance_type=instanceType, product_description=productDescript,
                availability_zone=availabilityZone)

        avgPrice = sum([i.price for i in ret]) / len(ret)
        bidPrice = avgPrice + 0.0001

        logging.info('Requesting spot instance %s with price %f...' % (instanceType, bidPrice))

        sirs = conn.request_spot_instances(price=bidPrice, image_id=imageId,
                user_data=userData,
                key_name=keyName, security_group_ids=securityGroupIds,
                instance_type=instanceType, placement=availabilityZone)

        logging.debug('Spot instance requests: %r' % sirs)

        if len(sirs) != 1:
            logging.error('Only one SIR is anticipated... (%r)' % sirs)
            cancelSpotInstanceRequests(conn, sirs)
            exit()

        jobInstId = pendForInstance(conn, [i.id for i in sirs])
        return jobInstId

    except KeyboardInterrupt:
        if sirs: cancelSpotInstanceRequests(conn, sirs)

        raise
    except:
        raise

if __name__ == '__main__':
    import os
    os.environ['AWS_ACCESS_KEY_ID'] = settings.aws_access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = settings.aws_secret_access_key

    conn = boto.ec2.connect_to_region(settings.region)
    requestSpotInstance(conn)

